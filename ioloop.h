#ifndef IOLOOP_H
#define IOLOOP_H 1

#define BUFFER_SIZE 32

#include "sim.h"

void ioloop(sim_t *sim);
void touch_page(sim_t *sim, unsigned int addr, char rw);

#endif
