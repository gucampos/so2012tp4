#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "fault.h"
#include "ioloop.h"
#include "sim.h"
#include "table.h"

void ioloop(sim_t *sim)
{
    int             read_cnt = 0;
    int             page_index = 0;
    unsigned int    addr;
    char            rw;
    page_t          pagina;
    page_table      tabela;

    printf("Abrindo arquivo de entrada %s...\n",sim->access_list);
    FILE * input = fopen(sim->access_list, "r");

    if (input==NULL) {
        perror("Impossivel abrir arquivo de entrada");
        exit(1);
    }

    /* Primeiro criamos nossa tabela de paginas */
    tabela.size = sim->mem_size/sim->page_size;
    tabela.tb = malloc(tabela.size * sizeof(frame_t));
    tabela.last = -1;

    int i;
    for(i=0;i<tabela.size;i++) tabela.tb[i] = frame_new();

    /* Print de informacoes uteis */
    printf("Tamanho da memoria: %4d Kb\n", sim->mem_size);
    printf("Tamanho de pagina:  %4d Kb\n", sim->page_size);
    printf("Tamanho da Tabela:  %4d frames\n", tabela.size);

    /* Agora lemos o arquivo de acessos, simulando cada um individualmente */
    puts("Executando simulacao, aguarde...");
    while (read_cnt!=EOF) {
        /* lemos enquanto tiver coisa pra ler */
        read_cnt = fscanf(input,"%x %c", &addr, &rw);
        if(read_cnt==EOF) break;

        /* Instanciando um acesso a pagina */
        pagina = page_new(sim->page_size, addr, rw);

        if(sim->debug) page_print(pagina);

        /* Buscamos pela pagina na tabela de paginas */
        page_index = get_page(&tabela, &pagina);

        /* Se nao estiver la, precisamos busca-la no disco */
        if(page_index==-1) {
            /* PAGE FAULT */
            page_index = page_fault(&tabela, &pagina, sim->alg, &(sim->written_pages));
            sim->page_faults++;
        }
        
        /* Se o page_fault retornou -1, abandona o barco */
        if(page_index==-1) {
            puts("Algoritmo nao implementado, adeus, bom dia");
            exit(1);
        }

        /* PAGE HIT */
        /* Se for acesso de escrita, marcamos a pagina como suja */
        if(pagina.is_write) {
            tabela.tb[page_index].modified = 1;
            sim->written_pages++;
        } 
        else {
            /* Se for somente leitura, basta incrementar o contador */
        }

        /* 
         *  Independente de leitura ou escrita, marcamos a pagina como
         *  acessada e incrementamos o contador
         */ 
        sim->access_count++;
        tabela.tb[page_index].last = time(NULL);
        tabela.last = page_index;
    } 

    fclose(input);
    free(tabela.tb);
}
