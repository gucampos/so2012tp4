#include <stdlib.h>
#include "fault.h"
#include "table.h"

int page_fault(page_table *tabela, page_t *pagina, char algoritmo, int *w)
{
    switch(algoritmo) {

        case LRU:
            return fault_lru(tabela,pagina,w);
            break;

        case FIFO:
            return fault_fifo(tabela,pagina,w);
            break;

        case RANDOM:
            return fault_random(tabela,pagina,w);
            break;
    }

    return -1;
}

int fault_fifo(page_table *tabela, page_t *pagina, int *w)
{
    /*
     * Acessamos diretamente a proxima pagina disponivel
     */
    int next = (tabela->last+1) % tabela->size;

    /*
     * Caso a pagina esteja suja, escrevemos no disco antes de substituir
     */
    if(tabela->tb[next].modified) {
        w++;
        tabela->tb[next].modified = 0;
    }

    /* Substituimos a pagina */
    tabela->tb[next].page = pagina->page_addr;

    /* Retornamos a nova pagina */
    return next;
}

int fault_random(page_table *tabela, page_t *pagina, int *w)
{
    /* Se existe alguma pagina limpa, eh ela mesmo que retornamos */
    int i;
    for(i=0;i<tabela->size;i++) {
        if(tabela->tb[i].page == -1) {
            tabela->tb[i].page = pagina->page_addr;
            return i;
        }
    }

    /* Se nenhuma pagina estava limpa, vamos pra substituicao */
    int next = (random() % tabela->size);

    /*
     * Caso a pagina esteja suja, escrevemos no disco antes de substituir
     */
    if(tabela->tb[next].modified) {
        w++;
        tabela->tb[next].modified = 0;
    }

    /* Substituimos a pagina */
    tabela->tb[next].page = pagina->page_addr;

    /* Retornamos a nova pagina */
    return next;
}

int fault_lru(page_table *tabela, page_t *pagina, int *w)
{
    /* Se existe alguma pagina limpa, eh ela mesmo que retornamos */
    int i;
    for(i=0;i<tabela->size;i++) {
        if(tabela->tb[i].page == -1) {
            tabela->tb[i].page = pagina->page_addr;
            return i;
        }
    }

    int next    =  0;
    time_t access  = 0;

    /* Se nenhuma pagina esta limpa, substituimos pela menos recentemente
     * utilizada 
     */

    access = tabela->tb[0].last;

    for(i=0;i<tabela->size;i++) {
        if (tabela->tb[i].last > access) {
            access = tabela->tb[i].last;
            next = i;
        }
    }

    return next;
}

