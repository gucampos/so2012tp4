#include <stdio.h>
#include "table.h"

page_t page_new(int page_size, unsigned int addr, char rw)
{
    page_t pagina;

    /* Primeiro determinamos se eh leitura ou escrita */
    pagina.is_write = (rw == READ) ? 0 : 1;

    /* Agora determinamos o endereco da pagina */
    unsigned int i      = 0;
    unsigned int t      = page_size;
    
    while(t>1) {
        t=t>>1;
        i++;
    }

    pagina.page_addr = addr >> i;

    return pagina;

}

void page_print(page_t pagina)
{
    printf("# WRITE[%u] # HEX[%8x] # DEC[%10u]\n", 
            pagina.is_write, pagina.page_addr, pagina.page_addr);
}

int get_page(page_table *tabela, page_t *pagina)
{
    int i;
    for(i=0;i<tabela->size;i++) {
        if(tabela->tb[i].page == pagina->page_addr) return i;
    }
    return -1;
}

frame_t frame_new()
{
    frame_t         frame;
    frame.page      = -1;
    frame.modified  = 0;
    frame.last      =0;
    return frame;
}
