SHELL = /bin/bash

CC = /usr/bin/gcc
FL = -g -Wall
TS = matriz.log

VALGRIND = /usr/bin/valgrind --leak-check=full --show-reachable=yes --track-origins=yes

tp4virtual: tp4virtual.o sim.o ioloop.o table.o  fault.o
	${CC} ${FL} -o $@ $^  -lm

tp4virtual.o: tp4virtual.c
	${CC} ${FL} -c -o $@ $<

ioloop.o: ioloop.c ioloop.h
	${CC} ${FL} -c -o $@ $<

sim.o: sim.c sim.h
	${CC} ${FL} -c -o $@ $<

table.o: table.c table.h
	${CC} ${FL} -c -o $@ $<

fault.o: fault.c fault.h
	${CC} ${FL} -c -o $@ $<

test: tp4virtual
	./$< lru testes/${TS} 4 128
	./$< fifo testes/${TS} 4 128
	./$< random testes/${TS} 4 128

valgrind: tp4virtual
	${VALGRIND} ./$<  lru testes/${TS} 4 128
	${VALGRIND} ./$<  fifo testes/${TS} 4 128
	${VALGRIND} ./$<  random testes/${TS} 4 128

desempenho_compilador_fixed_page: tp4virtual
	./$< fifo testes/compilador.log 4 128	
	./$< fifo testes/compilador.log 4 256
	./$< fifo testes/compilador.log 4 512	
	./$< fifo testes/compilador.log 4 1024	
	./$< random testes/compilador.log 4 128	
	./$< random testes/compilador.log 4 256
	./$< random testes/compilador.log 4 512	
	./$< random testes/compilador.log 4 1024	
	./$< lru testes/compilador.log 4 128	
	./$< lru testes/compilador.log 4 256
	./$< lru testes/compilador.log 4 512	
	./$< lru testes/compilador.log 4 1024	

desempenho_simulador_fixed_page: tp4virtual
	./$< fifo testes/simulador.log 4 128	
	./$< fifo testes/simulador.log 4 256
	./$< fifo testes/simulador.log 4 512	
	./$< fifo testes/simulador.log 4 1024	
	./$< random testes/simulador.log 4 128	
	./$< random testes/simulador.log 4 256
	./$< random testes/simulador.log 4 512	
	./$< random testes/simulador.log 4 1024	
	./$< lru testes/simulador.log 4 128	
	./$< lru testes/simulador.log 4 256
	./$< lru testes/simulador.log 4 512	
	./$< lru testes/simulador.log 4 1024	
	
desempenho_compressor_fixed_page: tp4virtual
	./$< fifo testes/compressor.log 4 128	
	./$< fifo testes/compressor.log 4 256
	./$< fifo testes/compressor.log 4 512	
	./$< fifo testes/compressor.log 4 1024	
	./$< random testes/compressor.log 4 128	
	./$< random testes/compressor.log 4 256
	./$< random testes/compressor.log 4 512	
	./$< random testes/compressor.log 4 1024	
	./$< lru testes/compressor.log 4 128	
	./$< lru testes/compressor.log 4 256
	./$< lru testes/compressor.log 4 512	
	./$< lru testes/compressor.log 4 1024	

desempenho_matriz_fixed_page: tp4virtual
	./$< fifo testes/matriz.log 4 128	
	./$< fifo testes/matriz.log 4 256
	./$< fifo testes/matriz.log 4 512	
	./$< fifo testes/matriz.log 4 1024	
	./$< random testes/matriz.log 4 128	
	./$< random testes/matriz.log 4 256
	./$< random testes/matriz.log 4 512	
	./$< random testes/matriz.log 4 1024	
	./$< lru testes/matriz.log 4 128	
	./$< lru testes/matriz.log 4 256
	./$< lru testes/matriz.log 4 512	
	./$< lru testes/matriz.log 4 1024	

desempenho_compilador_fixed_mem: tp4virtual
	./$< fifo testes/compilador.log 4 128	
	./$< fifo testes/compilador.log 8 128
	./$< fifo testes/compilador.log 16 128	
	./$< fifo testes/compilador.log 32 128	
	./$< fifo testes/compilador.log 64 128	
	./$< random testes/compilador.log 4 128	
	./$< random testes/compilador.log 8 128
	./$< random testes/compilador.log 16 128	
	./$< random testes/compilador.log 32 128	
	./$< random testes/compilador.log 64 128	
	./$< lru testes/compilador.log 4 128	
	./$< lru testes/compilador.log 8 128
	./$< lru testes/compilador.log 16 128	
	./$< lru testes/compilador.log 32 128	
	./$< lru testes/compilador.log 64 128	

desempenho_simulador_fixed_mem: tp4virtual
	./$< fifo testes/simulador.log 4 128	
	./$< fifo testes/simulador.log 8 128	
	./$< fifo testes/simulador.log 16 128	
	./$< fifo testes/simulador.log 32 128	
	./$< fifo testes/simulador.log 64 128	
	./$< random testes/simulador.log 4 128	
	./$< random testes/simulador.log 8 128	
	./$< random testes/simulador.log 16 128	
	./$< random testes/simulador.log 32 128	
	./$< random testes/simulador.log 64 128	
	./$< lru testes/simulador.log 4 128	
	./$< lru testes/simulador.log 8 128	
	./$< lru testes/simulador.log 16 128	
	./$< lru testes/simulador.log 32 128	
	./$< lru testes/simulador.log 64 128	
	
desempenho_compressor_fixed_mem: tp4virtual
	./$< fifo testes/compressor.log 4 128	
	./$< fifo testes/compressor.log 8 128	
	./$< fifo testes/compressor.log 16 128	
	./$< fifo testes/compressor.log 32 128	
	./$< fifo testes/compressor.log 64 128	
	./$< random testes/compressor.log 4 128	
	./$< random testes/compressor.log 8 128	
	./$< random testes/compressor.log 16 128	
	./$< random testes/compressor.log 32 128	
	./$< random testes/compressor.log 64 128	
	./$< lru testes/compressor.log 4 128	
	./$< lru testes/compressor.log 8 128	
	./$< lru testes/compressor.log 16 128	
	./$< lru testes/compressor.log 32 128	
	./$< lru testes/compressor.log 64 128	

desempenho_matriz_fixed_mem: tp4virtual
	./$< fifo testes/matriz.log 4 128	
	./$< fifo testes/matriz.log 8 128	
	./$< fifo testes/matriz.log 16 128	
	./$< fifo testes/matriz.log 32 128	
	./$< fifo testes/matriz.log 64 128	
	./$< random testes/matriz.log 4 128	
	./$< random testes/matriz.log 8 128	
	./$< random testes/matriz.log 16 128	
	./$< random testes/matriz.log 32 128	
	./$< random testes/matriz.log 64 128	
	./$< lru testes/matriz.log 4 128	
	./$< lru testes/matriz.log 8 128	
	./$< lru testes/matriz.log 16 128	
	./$< lru testes/matriz.log 32 128	
	./$< lru testes/matriz.log 64 128	

clean:
	if [ -e tp4virtual ]; then rm tp4virtual; fi && rm -rf *.o


.PHONY: clean
