#ifndef FAULT_H 
#define FAULT_H 1

#ifndef LRU 
#define LRU 'l'
#endif

#ifndef FIFO
#define FIFO 'f'
#endif

#ifndef RANDOM 
#define RANDOM 'r'
#endif

#include "table.h"

/*
 * Funcao wrapper que retorna uma pagina valida realizando o page fault
 * com um dos algoritmos implementados
 *
 * Se a casa cair retorna -1 e o mundo que se exploda
 */
int page_fault(page_table *tabela, page_t *pagina, char algoritmo, int *w);

int fault_fifo(page_table *tabela, page_t *pagina, int *w);
int fault_random(page_table *tabela, page_t *pagina, int *w);
int fault_lru(page_table *tabela, page_t *pagina, int *w);
#endif
