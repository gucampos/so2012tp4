#ifndef TABLE_H
#define TABLE_H 1

#include <time.h>

#define READ 'R'
#define WRITE 'W'

/* 
 * Estrutura que define uma pagina de memoria, do ponto de vista de acesso
 */
typedef struct {
    unsigned int page_addr;
    unsigned int is_write;
} page_t;

/*
 * Estrutura que define um frame da memoria / tabela de paginas 
 */
typedef struct {
    unsigned int page;      // Pagina referenciada
    unsigned int modified;  // Pagina modificada?
    time_t       last;      // Tempo em que foi acessada por ultimo
} frame_t;

/*
 * Estrutura que define a tabela de paginas
 */
typedef struct {
    frame_t *tb;    // A tabela de paginas propriamente dita;
    int     size;   // Shortcut para o tamanho da tabela 
    int     last;   // Ultimo frame acessado (para FIFO)
} page_table;

/*
 * Instancia um novo acesso a pagina de memoria
 */
page_t page_new(int page_size, unsigned int addr, char rw); 
frame_t frame_new();

/*
 * Imprime dados de um acesso a memoria
 */
void page_print(page_t pagina);

/*
 * Busca por uma pagina na tabela de paginas, retorna o index da pagina na
 * tabela ou -1 caso nao esteja la
 */
int get_page(page_table *tabela, page_t *pagina);
#endif
