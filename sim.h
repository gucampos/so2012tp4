#ifndef SIM_H
#define SIM_H

#define MIN_PAGE_SIZE   2
#define MAX_PAGE_SIZE   64
#define MIN_MEM         128
#define MAX_MEM         16384

#ifndef LRU
#define LRU 'l'
#endif

#ifndef FIFO
#define FIFO 'f'
#endif

#ifndef RANDOM
#define RANDOM 'r'
#endif

typedef struct {

    char alg;               // Qual algoritmo utilizar
    char *access_list;      // Arquivo com a lista de acessos
    int page_size;          // Tamanho da pagina
    int mem_size;            // Memoria maxima do processo
    int debug;              // Debug?
    int access_count;       // Contagem de acessos
    int page_faults;        // Contagem de page faults
    int written_pages;      // Contagem de escritas

} sim_t;

int sim_new(int argc, char **argv, sim_t *instance);
void print_sim(sim_t *sim);

#endif
