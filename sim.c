#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sim.h"

int sim_new(int argc, char **argv, sim_t *instance)
{   
    /* TIPO DE ALGORITMO */

    if(!strcmp(argv[1],"lru")) instance->alg= LRU;
    else if(!strcmp(argv[1],"fifo")) instance->alg= FIFO;
    else if(!strcmp(argv[1],"random")) instance->alg= RANDOM;
    else {
        puts("Algoritmo de substituicao de paginas indisponivel");
        return 1;
    }

    printf("Algoritmo de reposicao: %s\n", argv[1]);

    /* ARQUIVO DE ENTRADA */
    instance->access_list = argv[2];

    /* TAMANHO DE PAGINA */
    instance->page_size = atoi(argv[3]);

    if ( (instance->page_size<MIN_PAGE_SIZE) || (instance->page_size>MAX_PAGE_SIZE) ) {
        puts("Tamanho de pagina nao suportado");
        printf("Minimo: %d\nMaximo: %d\n",MIN_PAGE_SIZE, MAX_PAGE_SIZE);
        return 1;
    }

    /* TAMANHO DA MEMORIA */
    instance->mem_size = atoi(argv[4]);

    if ( (instance->mem_size<MIN_MEM) || (instance->mem_size>MAX_MEM) ){
        puts("Tamanho da memoria nao suportado");
        printf("Minimo: %d\nMaximo: %d\n",MIN_MEM, MAX_MEM);
        return 1;
    }

    if(argc==6) instance->debug = 1;
    else instance->debug = 0;
    instance->access_count   = 0;
    instance->page_faults    = 0;
    instance->written_pages  = 0;

    return 0;
}
