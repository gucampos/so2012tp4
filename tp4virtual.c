#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "sim.h"
#include "ioloop.h"

int main (int argc, char **argv) {

    sim_t sim;

    /* Tratamento de Argumentos */
    if ( (argc<5) || (argc>6) ) {
        puts("Numero incorreto de argumentos"); 
        exit(1);
    }

    int success = sim_new(argc, argv, &sim);
    if(success!=0) return 1;    

    ioloop(&sim);

    puts("\nResultados:");
    printf("Paginas acessadas: %8d\n", sim.access_count);
    printf("Paginas lidas:     %8d\n", sim.access_count-sim.written_pages);
    printf("Paginas escritas:  %8d\n", sim.written_pages);
    printf("Page faults:       %8d\n", sim.page_faults);

    return 0;
}
